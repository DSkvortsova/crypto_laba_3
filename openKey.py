from random import randint

def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a


def coprime(a, b):
    return gcd(a, b) == 1


def genOpenKey(closeKey: []):
    m = sum(closeKey) + randint(1, 50)
    res = []
    res.append(m)
    for i in range(2,m):
        flag = coprime(i,m)
        if(flag == True):
            n = i
            break
    res.append(n)
    opK = []
    for k in range(len(closeKey)):
        opK.append((closeKey[k] * n) % m)
    res.append(opK)
    # print(res)
    return res