import math


def decode(encodText: [], n:int, m:int, closeKey: []):
    result = []
    a = pow(n, -1, m)
    # print('n', n)
    # print('обратное', a, 'по модулю', m)
    indexArray = []
    for i in range(len(encodText)):
        tmp = (encodText[i] * a) % m
        # print(tmp)
        for k in range(len(closeKey))[::-1]:
            if(tmp >= closeKey[k]):
                tmp = tmp-closeKey[k]
                indexArray.append(closeKey.index(closeKey[k])+1)
                # indexArray.append(closeKey[k] + 1)
            else:
                continue
        indexArray.append('*')
    temp = 0
    # print(indexArray)
    for j in range(len(indexArray)):
        if(indexArray[j] != '*'):
            temp = math.pow(2, len(closeKey)-indexArray[j]) + temp
        else:
            result.append(f'{int(temp):08b}')
            # result.append(temp)
            temp = 0
    return ''.join(result)