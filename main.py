import os
from dotenv import load_dotenv
from openKey import genOpenKey
from encod import encode
from decod import decode


load_dotenv()

text = os.getenv("TEXT", "Hello, World!")
# text = 'Lions are big, strong animals that live in some parts of the world.'
bin_text = ''.join(format(ord(x), '08b') for x in text)
n = 8
print('Исходный текст сообщения: ', text)
print('Переведенный текст в 2ю сс: ', bin_text)
closeKey = [1, 2, 4, 8, 16, 32, 64, 128]
openKey = genOpenKey(closeKey)
print("Open Key is ", openKey[2])
encodeText = encode(openKey[2], bin_text, n)
print('Зашифрованное сообщение: ', encodeText)
decodeText = decode(encodeText, openKey[1], openKey[0], closeKey)
print('Расшифрованное сообщение в 2й сс: ', decodeText)
print('Расшифрованное сообщение: ', ''.join(chr(int(x, 2)) for x in map(''.join, zip(*[iter(decodeText)] * 8))))



