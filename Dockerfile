FROM python:3.12

ENV TEXT "Hello!"


COPY main.py main.py
COPY decod.py decod.py
COPY encod.py encod.py
COPY openKey.py openKey.py

RUN pip install python-dotenv
CMD ["python", "main.py"]