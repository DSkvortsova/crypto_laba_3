def grouper(iterable, n):
    args = [iter(iterable)] * n
    return zip(*args)


def encode(openKey: [],text:str, n:int):
    result = []
    l = [''.join(i) for i in grouper(text, n)]
    for k in range(len(l)):
        tmp = l[k]
        res = 0
        for i in range(len(openKey)):
            res += openKey[i]*int(tmp[i])
        result.append(res)
    return result